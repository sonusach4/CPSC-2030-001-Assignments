USE pokedex;
SELECT * FROM pokemon ORDER BY SPD DESC LIMIT 10;
SELECT * FROM pokemon,vulnerable_to,resistant_to WHERE pokemon.TYPE=vulnerable_to.TYPE AND pokemon.TYPE=resistant_to.TYPE AND vulnerable_to.TYPE2="GROUND" AND resistant_to.TYPE2="STEEL";
SELECT * FROM pokemon FULL OUTER JOIN weak_against ON pokemon.TYPE=weak_against.TYPE WHERE BST>=200 AND BST<=500 AND weak_against.TYPE2="WATER";
SELECT * FROM pokemon FULL OUTER JOIN vulnerable_to ON pokemon.TYPE=vulnerable_to.TYPE WHERE NAME LIKE '%MEGA %' ORDER BY ATK DESC LIMIT 1 AND vulnerable_to.TYPE2="FIRE";