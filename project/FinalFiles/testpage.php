<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
    <script src="https://use.fontawesome.com/90dc64b761.js"></script>
    <link rel="stylesheet/less" type="text/css" media="screen" href="styles.less" />
    <script src="script.js" defer></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/less.js/3.0.2/less.min.js" ></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <link rel="icon" type="image/x-icon"  href="Walsh-icon.png">
    <script type="text/javascript">
      var pos=0;
      $(window).ready(function(){

        $(".left").click(function(){
          if(pos>0)
          {
            pos=pos-1;
            $(".move").animate({"left":"-"+pos*98.4+"%"});
          }
        });

        $(".right").click(function(){
          if(pos<4)
          {
            pos=pos+1;
            $(".move").animate({"left":"-"+pos*98.4+"%"});
          }
        });
      });
    </script>
    <title>Header</title>
  </head>
  <body>

    <div class="container">
      <header>
        <div class="heading">
          <h1> Walsh Institute</h1>
          <h3><em>"A Transforming, Healing Presence"</em></h3>
        </div>
      </header>

      <nav>
        <a href="index.php"><i class="fas fa-home" aria-hidden="true"></i>Home</a>
        <a href="med-procedures.php"><i class="fas fa-notes-medical" aria-hidden="true"></i>Medical Procedures</a>
        <a href="our-team.php"><i class="fas fa-user-md" aria-hidden="true"></i>Our Team</a>
        <a href="contact-us.php"><i class="fas fa-address-book" aria-hidden="true"></i>Contact Us</a>
      </nav>

      <section class="login">
        <a href="patient-login.php">Patient Login</a>
        <a href="doctor-login.php">Doctor Login</a>
      </section>

      <main class="page">
        <h1>Our Team</h1>
        <section class="slide">

          <div class="contain">

            <div class="move">
              <img src="doctor1.jpeg" alt="Dr. Sourabh Sachdeva">
              <img src="doctor2.jpeg" alt="Dr. Prithvi Verma">
              <img src="doctor3.jpeg" alt="Dr. Gagandeep Singh">
              <img src="doctor4.jpeg" alt="Dr. Harpreet Sangha">
              <img src="doctor5.jpeg" alt="Dr. Narinder Singh">
            </div>

          </div>
          <span class="left"><i class="fa fa-angle-double-left" aria-hidden="true"></i>
          </span>
          <span class="right"><i class="fa fa-angle-double-right" aria-hidden="true"></i>
          </span>
        </section>

        <section class="team">
          <img src="doctor1.jpeg" alt="Doctor Image">
          <ul>
            <li>Name: Sourabh Sachdeva</li>
            <li>Degree: M.D. Medicine</li>
            <li>Department: Medicine</li>
            <li>Phone: 77821345876</li>
          </ul>
        </section>

        <section class="team">
          <img src="doctor2.jpeg" alt="Doctor Image">
          <ul>
            <li>Name: Prithvi Verma</li>
            <li>Degree: M.S.</li>
            <li>Department: Oncology</li>
            <li>Phone: 7784567891</li>
          </ul>
        </section>
        <section class="team">
          <img src="doctor3.jpeg" alt="Doctor Image">
          <ul>
            <li>Name: Gagandeep Singh</li>
            <li>Degree: M.B.B.S.</li>
            <li>Department: E.N.T</li>
            <li>Phone: 7781234567</li>
          </ul>
        </section>
        <section class="team">
          <img src="doctor4.jpeg" alt="Doctor Image">
          <ul>
            <li>Name: Harpreet Singh</li>
            <li>Degree: M.B.B.S.</li>
            <li>Department: Oncology</li>
            <li>Phone: 7782345678</li>
          </ul>
        </section>
        <section class="team">
          <img src="doctor5.jpeg" alt="Doctor Image">
          <ul>
            <li>Name: Narinder Singh</li>
            <li>Degree: M.D.</li>
            <li>Department: Cardiology</li>
            <li>Phone: 7783456789</li>
          </ul>
        </section>
      </main>

      <footer>Copyright (c) 2018 Copyright Holder All Rights Reserved.</footer>
    </div>

  </body>
</html>
