<?php
  session_start()
?>

<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
    <script src="https://use.fontawesome.com/90dc64b761.js"></script>
    <link rel="stylesheet/less" type="text/css" media="screen" href="styles.less" />
    <script src="script.js" defer></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/less.js/3.0.2/less.min.js" ></script>
    <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
        crossorigin="anonymous"></script>
    <link rel="icon" type="image/x-icon"  href="Walsh-icon.png">
    <!-- <script>
      $(document).ready(function() {
        $("form").submit(function(event) {
          event.preventDefault();
          var problem = $("problem").val();
          var doctor = $("doctor").val();
          var comment = $("comment").val();
          $(".message").load("appointment.php", { problem: problem, doctor: doctor, comment: comment });
        });
      });
    </script> -->
    <script>
      $(document).ready(function() {
        $.ajax({
          url:"appointment.php",
          method:"POST",
          dataType:"JSON",
          success:function(data) {
            $('#doctor_username').text(data.doctor);
            $('#patient_username').text(data.patient);
            $('#problem').text(data.problem);
            $('#day').text(data.day);
            $('#comment').text(data.comment);
          }
        })
      });
    </script>
    <title>Book Appointment - Walsh Institute</title>
  </head>
  <body>

    <header>
      <div class="heading">
        <h1> Walsh Institute</h1>
        <h3><em>"A Transforming, Healing Presence"</em></h3>
      </div>
    </header>

    <nav>
      <a href="patient-login-success.php"><i class="far fa-calendar-check" aria-hidden="true"></i>Book Appointment</a>
      <a href="patient-appointments.php"><i class="far fa-calendar-check" aria-hidden="true"></i>My Appointments</a>
      <a href="details.php"><i class="fas fa-user-md" aria-hidden="true"></i>Update Profile</a>
      <a href="patient-contact.php"><i class="fas fa-address-book" aria-hidden="true"></i>Contact Us</a>
    </nav>

    <section class="login">
      <a href="#">Welcome <?php echo $_SESSION['firstname']." ".$_SESSION['lastname'] ?></a>

      <form class="logout-button" action="logout.inc.php" method="post">
        <button type="submit" name="logout-button">Logout</button>
      </form>

    </section>

    <main class="page appointment-form">
      <div class="form-container">
        <form class="" action="appointment.php" method="post">
          <!-- <label for="patient-first-name">First Name</label>
          <input type="text" name="patient-first-name" placeholder="Firstname"> -->

          <label for="patient-problem">Problem</label>
          <input id="problem" type="text" name="patient-problem" placeholder="Problem">

          <label for="doctors">Doctors</label>
          <select id="doctor" class="" name="doctors">
            <option value="sonusach98">Dr. Sourabh Sachdeva</option>
            <option value="prithvi98">Dr. Prithvi Verma</option>
            <option value="gagan98">Dr. Gagandeep Singh</option>
            <option value="narinder98">Dr. Narinder Singh</option>
            <option value="harry98">Dr. Harry Sangha</option>
          </select>

          <label for="day">Day</label>
          <input type="text" name="day" placeholder="Day">

          <label for="comments">Comments</label>
          <textarea id="comment" name="comments" rows="8" cols="80">Enter your comments here</textarea>
          <button type="submit" name="book-appointment">Book Appointment</button>

          <section class="results">
            <table>
              <tr>
                <td>Doctor ID</td>
                <td><span id="#doctor_username"></span></td>
              </tr>
              <tr>
                <td>Patient ID</td>
                <td><span id="#patient_username"></span></td>
              </tr>
              <tr>
                <td>Problem</td>
                <td><span id="#problem"></span></td>
              </tr>
              <tr>
                <td>Day</td>
                <td><span id="#day"></span></td>
              </tr>
              <tr>
                <td>Comments</td>
                <td><span id="#comments"></span></td>
              </tr>
            </table>
          </section>
        </form>
      </div>


    </main>

  </body>
</html>
