<?php
  session_start();
  if(isset($_POST['change-details'])) {
    require'db.inc.php';

    $firstname = $_POST['firstname'];
    $lastname = $_POST['lastname'];
    $phone = $_POST['phone'];
    $degree = $_POST['degree'];
    $department = $_POST['department'];
    $username = $_SESSION['username'];
    // In this scenario I am forcing the user to update all his details
    echo $_SESSION['username'];

    if (!preg_match("/^[0-9]*$/", $phone)) {
      header("Location: doctor-details.php?error=invalid-phone");
      exit();
    }

    $sql = "UPDATE doctor SET firstname='$firstname', lastname='$lastname', phone='$phone', degree='$degree', department='$department' WHERE username='$username'";
    $result = mysqli_query($conn, $sql);
    if($result) {
      header("Location: doctor-details.php?success=Record-updated-successfully");
    } else {
      header("Location: doctor-details.php?error=sql-error");
    }
  } else {
    header("Location: doctor-details.php");
    exit;
  }

?>
