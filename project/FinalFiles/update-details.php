<?php
  session_start();
  if(isset($_POST['change-details'])) {
    require'db.inc.php';

    $firstname = $_POST['firstname'];
    $lastname = $_POST['lastname'];
    $phone = $_POST['phone'];
    $address = $_POST['address'];
    $username = $_SESSION['username'];
    // In this scenario I am forcing the user to update all his details
    echo $_SESSION['username'];

    if (!preg_match("/^[0-9]*$/", $phone)) {
      header("Location: details.php?error=invalid-phone");
      exit();
    }

    $sql = "UPDATE patient SET firstname='$firstname', lastname='$lastname', phone='$phone', address='$address' WHERE username='$username'";
    $result = mysqli_query($conn, $sql);
    if($result) {
      header("Location: details.php?success=Record-updated-successfully");
    } else {
      header("Location: details.php?error=sql-error");
    }
  } else {
    header("Location: details.php");
    exit;
  }

?>
