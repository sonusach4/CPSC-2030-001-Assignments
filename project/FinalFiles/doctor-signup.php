<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet/less" type="text/css" media="screen" href="login.less" />
    <link rel="stylesheet/less" type="text/css" media="screen" href="styles.less" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/less.js/3.7.1/less.min.js"></script>
    <title>Doctor Signup - Walsh Institute</title>
  </head>
  <body>

    <main>

      <section class="login">
        <a href="index.php">Homepage</a>
      </section>

      <div class="container">
        <h2>Signup</h1>
        <div class="signup-form">
          <form action="doctor-signup.inc.php" method="post">
            <label for="userame"><b>Username</b></label>
            <input type="text" name="username" placeholder="Username" required>

            <label for="firstname"><b>First Name</b></label>
            <input type="text" name="firstname" placeholder="First Name" required>

            <label for="lastname"><b>Last Name</b></label>
            <input type="text" name="lastname" placeholder="Last Name" required>

            <label for="phone"><b>Phone Number</b></label>
            <input type="text" name="phone" placeholder="Phone Number" required>

            <label for="degree"><b>Degree</b></label>
            <input type="text" name="degree" placeholder="Degree" required>

            <label for="department"><b>Department</b></label>
            <input type="text" name="department" placeholder="Department" required>

            <label for="password"><b>Password</b></label>
            <input type="password" name="password" placeholder="Password" required>

            <label for="checkpassword"><b>Password Again</b></label>
            <input type="password" name="checkpassword" placeholder="Enter your password again" required>

            <button type="submit" name="signup">Sign Up</button>
            <a class="signup-link" href="doctor-login.php">Login</a>
          </form>
        </div>
      </div>

    </main>
  </body>
</html>
