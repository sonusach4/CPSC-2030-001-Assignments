<?php
  session_start();
?>

<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
    <script src="https://use.fontawesome.com/90dc64b761.js"></script>
    <link rel="stylesheet/less" type="text/css" media="screen" href="styles.less" />
    <script src="script.js" defer></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/less.js/3.0.2/less.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
        crossorigin="anonymous"></script>
    <link rel="icon" type="image/x-icon"  href="Walsh-icon.png">
    <title>Profile - Walsh Institute</title>
  </head>
  <body>

    <header>
      <div class="heading">
        <h1> Walsh Institute</h1>
        <h3><em>"A Transforming, Healing Presence"</em></h3>
      </div>
    </header>

    <nav>
      <a href="doctor-login-success.php"><i class="fas fa-user-md" aria-hidden="true"></i>Details</a>
      <a href="doctor-appointments.php"><i class="far fa-calendar-check" aria-hidden="true"></i>My Appointments</a>
      <a href="doctor-details.php"><i class="fas fa-user-md" aria-hidden="true"></i>Update Profile</a>
      <a href="doctor-contact.php"><i class="fas fa-address-book" aria-hidden="true"></i>Contact Us</a>
    </nav>

    <section class="login">
      <a href="#">Welcome <?php echo $_SESSION['firstname']." ".$_SESSION['lastname'] ?></a>

      <form class="logout-button" action="logout.inc.php" method="post">
        <button type="submit" name="logout-button">Logout</button>
      </form>

    </section>

    <main class="page appointment-form">
      <div class="form-container">
        <form action="update-doctor-details.php" method="post">
          <label class="firstname" for="firstname"><b>First Name</b></label>
          <input class="firstname" type="text" name="firstname" placeholder="First Name" required>

          <label class="lastname" for="lastname"><b>Last Name</b></label>
          <input class="lastname" type="text" name="lastname" placeholder="Last Name" required>

          <label class="phone" for="phone"><b>Phone Number</b></label>
          <input class="phone" type="text" name="phone" placeholder="Phone Number" required>

          <label class="degree" for="degree"><b>Degree</b></label>
          <input class="degree" type="text" name="degree" placeholder="Degree" required>

          <label class="department" for="department"><b>Department</b></label>
          <input class="department" type="text" name="department" placeholder="Department" required>

          <button type="submit" name="change-details">Update</button>
      </div>


      </form>
    </main>

  </body>
</html>
