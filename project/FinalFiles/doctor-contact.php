<?php
  session_start()
?>

<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
    <script src="https://use.fontawesome.com/90dc64b761.js"></script>
    <link rel="stylesheet/less" type="text/css" media="screen" href="styles.less" />
    <script src="script.js" defer></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/less.js/3.0.2/less.min.js" ></script>
    <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
        crossorigin="anonymous"></script>
    <link rel="icon" type="image/x-icon"  href="Walsh-icon.png">
    <!-- <script>
      $(document).ready(function() {
        $("form").submit(function(event) {
          event.preventDefault();
          var problem = $("problem").val();
          var doctor = $("doctor").val();
          var comment = $("comment").val();
          $(".message").load("appointment.php", { problem: problem, doctor: doctor, comment: comment });
        });
      });
    </script> -->
    <script src="slideshow.js" defer></script>
    <title>Book Appointment - Walsh Institute</title>
  </head>
  <body>

    <header>
      <div class="heading">
        <h1> Walsh Institute</h1>
        <h3><em>"A Transforming, Healing Presence"</em></h3>
      </div>
    </header>

    <nav>
      <a href="doctor-login-success.php"><i class="fas fa-user-md" aria-hidden="true"></i>Details</a>
      <a href="doctor-appointments.php"><i class="far fa-calendar-check" aria-hidden="true"></i>My Appointments</a>
      <a href="doctor-details.php"><i class="fas fa-user-md" aria-hidden="true"></i>Update Profile</a>
      <a href="doctor-contact.php"><i class="fas fa-address-book" aria-hidden="true"></i>Contact Us</a>
    </nav>

    <section class="login">
      <a href="#">Welcome <?php echo $_SESSION['firstname']." ".$_SESSION['lastname'] ?></a>

      <form class="logout-button" action="logout.inc.php" method="post">
        <button type="submit" name="logout-button">Logout</button>
      </form>

    </section>

    <main class="contact">
      <h1>Contact</h1>

      <section class="myinfo">
        <h2>Owner Info</h2>
        <p><strong>Dr. Malcolm Walsh</strong></p>

        <p>E-Mail Address : </p>
        <a class="fbk" href="mailto:malcolmwalsh@gmail.com">malcolmwalsh@gmail.com</a>
        <p>Phone : 1234567890</p>


        <div class="address">
          <p id="p1">Home Address</p>
          <p>H.NO 123, ABCD</p>
          <p>XYZ STREET</p>
          <p>ASDF</p>

        </div>
      </section>
      <section class="forms">
        <h2>Your Details :</h2>
        <form class="contact-form" action="http://webdevbasics.net/scripts/demo.php" method="post">

          <fieldset class="pinfo">
            <legend>Personal Information</legend>
            <label for="asd">First Name :
              <input id="asd" type="text" name="First Name"><br><br>
            </label>
            <label for="xyz">Last Name :
              <input id="xyz" type="text" name="Last Name"><br><br>
            </label>
            <label for="abc">
              Enter Your E-Mail Here :
              <input id="abc" type="text" name="Sonu" value="">
            </label>
          </fieldset>

          <fieldset class="oinfo">
            <legend>Other Information</legend>
            <p>Where did you hear about this website? Choose that apply :</p>

            <label>
            <input type="radio" name="source" value="Facebook">Facebook <br>
            </label>

            <label>
            <input type="radio" name="source" value="Instagram">Instagram <br>
            </label>

            <label>
            <input type="radio" name="source" value="Friends">Friends <br>
            </label>

            <label>
            <input type="radio" name="source" value="Other Source">Other Source <br>
            </label>

            <label><br>Enter your Comments Here :
              <br> <textarea name="Comments" rows="8" cols="80"></textarea>
            </label>
          </fieldset>


            <br>
          <input type="submit" name="Submit">
        </form>

      </section>
    </main>

    <footer>Copyright (c) 2018 Copyright Holder All Rights Reserved.</footer>
  </body>
</html>
