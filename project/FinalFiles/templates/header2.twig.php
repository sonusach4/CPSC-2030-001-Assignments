<?php
session_start();
?>

<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
    <script src="https://use.fontawesome.com/90dc64b761.js"></script>
    <link rel="stylesheet/less" type="text/css" media="screen" href="styles.less" />
    <script src="script.js" defer></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/less.js/3.0.2/less.min.js" ></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <link rel="icon" type="image/x-icon"  href="Walsh-icon.png">
    <title>Homepage - Walsh Institute</title>
  </head>
  <body>
    <header>
      <div class="heading">
        <h1> {{ mainTitle }}</h1>
        <h3><em>{{ subTitle }}</em></h3>
      </div>
    </header>

    <nav>
      <a href="index.php"><i class="fas fa-home" aria-hidden="true"></i>Home</a>
      <a href="med-procedures.php"><i class="fas fa-notes-medical" aria-hidden="true"></i>Medical Procedures</a>
      <a href="our-team.php"><i class="fas fa-user-md" aria-hidden="true"></i>Our Team</a>
      <a href="contact-us.php"><i class="fas fa-address-book" aria-hidden="true"></i>Contact Us</a>
    </nav>

    <section class="login">
      <a href="#">Welcome <?php echo $_SESSION['username'] ?></a>

      <form class="logout-button" action="logout.inc.php" method="post">
        <button type="submit" name="logout-button">Logout</button>
      </form>
      
    </section>
  </body>
</html>
