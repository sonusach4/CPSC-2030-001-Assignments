<?php
require_once './vendor/autoload.php';

$loader = new Twig_Loader_Filesystem('./templates'); //set to load from the ./templates directory

$twig = new Twig_Environment($loader);

$template = $twig->load("login.twig.html");
    echo $template->render(array('pageTitle' => 'Doctor Login',
                                 'phpPage' => 'doctor-login.inc.php',
                                 'nameLogin' => 'Doctor',
                                 'signupPage' => 'doctor-signup.php'));

?>
