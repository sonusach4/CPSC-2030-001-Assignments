<?php
  // Checking if the user pressed the signup button
  if (isset($_POST['signup'])) {
    // Making a connection with the database
    require'db.inc.php';
    // Getting the values from the form and storing them into variables
    $username = $_POST['username'];
    $firstname = $_POST['firstname'];
    $lastname = $_POST['lastname'];
    $phone = $_POST['phone'];
    $degree = $_POST['degree'];
    $department = $_POST['department'];
    $password = $_POST['password'];
    $passwordRepeat = $_POST['checkpassword'];

    // Used to check if any of the fields are Empty
    // But now used 'requied' keyword in the input fileds

    // if (empty($username) || empty($firstname) || empty($lastname) || empty($phone) || empty($address) || empty($password) || empty($passwordRepeat)) {
    //    header("Location: patient-signup.php?error=empty-fields&username=".$username."&firstname=".$firstname."&lastname=".$lastname."&phone=".$phone."&address=".$address);
    //    exit();
  //}

    // Checking if there are any characters in the user name other than alphabets and digits
    if (!preg_match("/^[a-zA-Z0-9]*$/", $username)) {
      header("Location: doctor-signup.php?error=invalid-username&firstname=".$firstname."&lastname=".$lastname."&phone=".$phone."&address=".$address);
      exit();
    }
    // Checking if the phone number consists anything other than digits
    else if (!preg_match("/^[0-9]*$/", $phone)) {
      header("Location: doctor-signup.php?error=invalid-phone&username=".$username."&firstname=".$firstname."&lastname=".$lastname."&address=".$address);
      exit();
    }
    // Checking if both the passwords match
    else if ($password !== $passwordRepeat) {
      header("Location: doctor-signup.php?error=password-does-not-match&username=".$username."&firstname=".$firstname."&lastname=".$lastname."&phone=".$phone."&address=".$address);
      exit();
    }
    // If no errors then get the data from database
    else {
      // $sql = "SELECT username FROM patient WHERE username=".$username;
      $sql = "SELECT username FROM doctor WHERE username=?";
      $stmt = mysqli_stmt_init($conn);

      if (!mysqli_stmt_prepare($stmt, $sql)) {
        header("Location: doctor-signup.php?error=sql-error1");
        exit();
      }
      else {
        mysqli_stmt_bind_param($stmt, "s", $username);
        mysqli_stmt_execute($stmt);
        mysqli_stmt_store_result($stmt);

        $result = mysqli_stmt_num_rows($stmt);
        if ($result > 0) {
          header("Location: doctor-signup.php?error=user-already-exists&firstname=".$firstname."&lastname=".$lastname."&phone=".$phone."&address=".$address);
          exit();
        }
        else {
          $sql = "INSERT INTO doctor (username,firstname,lastname,phone,degree,department,password) VALUES (?, ?, ?, ?, ? ,? ,?)";
          $stmt = mysqli_stmt_init($conn);
          if (!mysqli_stmt_prepare($stmt, $sql)) {
            header("Location: doctor-signup.php?error=sql-error2");
            exit();
          }
          else {
            $hashPassword = password_hash($password, PASSWORD_DEFAULT);
            mysqli_stmt_bind_param($stmt, "sssssss", $username, $firstname, $lastname, $phone, $degree, $department, $hashPassword);
            mysqli_stmt_execute($stmt);
            header("Location: doctor-login.php?signup=success");
            exit();
          }
        }
      }
    }
    mysqli_stmt_close($stmt);
    mysqli_close($conn);
  }
  else {
    header("Location: doctor-signup.php");
    exit();
  }
