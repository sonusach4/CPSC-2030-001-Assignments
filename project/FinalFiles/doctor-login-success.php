<?php
session_start();
?>

<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
    <script src="https://use.fontawesome.com/90dc64b761.js"></script>
    <link rel="stylesheet/less" type="text/css" media="screen" href="styles.less" />
    <script src="script.js" defer></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/less.js/3.0.2/less.min.js" ></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <link rel="icon" type="image/x-icon"  href="Walsh-icon.png">
    <!-- <script type="text/javascript">
      $(document).ready(function() {
        $(a).hover(function() {
          $(this).find('section>button').fadeToggle(200);
        });
      });
    </script> -->
    <title>Details - Walsh Institute</title>
  </head>
  <body>
    <header>
      <div class="heading">
        <h1> Walsh Institute</h1>
        <h3><em>"A Transforming, Healing Presence"</em></h3>
      </div>
    </header>

    <nav>
      <a href="doctor-login-success.php"><i class="fas fa-user-md" aria-hidden="true"></i>Details</a>
      <a href="doctor-appointments.php"><i class="far fa-calendar-check" aria-hidden="true"></i>My Appointments</a>
      <a href="doctor-details.php"><i class="fas fa-user-md" aria-hidden="true"></i>Update Profile</a>
      <a href="doctor-contact.php"><i class="fas fa-address-book" aria-hidden="true"></i>Contact Us</a>
    </nav>

    <section class="login">
      <a href="#">Welcome <?php echo $_SESSION['firstname']." ".$_SESSION['lastname'] ?></a>

      <form class="logout-button" action="logout.inc.php" method="post">
        <button type="submit" name="logout-button">Logout</button>
      </form>

    </section>

    <main class="page">

      <h1>Welcome <?php echo $_SESSION['firstname']; ?></h1>
      <h2>Username:
        <?php echo $_SESSION['username']; ?>
      </h2>

      <h2>Firstname:
        <?php echo $_SESSION['firstname']; ?>
      </h2>

      <h2>Lastname:
        <?php echo $_SESSION['lastname']; ?>
      </h2>

      <h2>Phone:
        <?php echo $_SESSION['phone']; ?>
      </h2>

      <h2>Degree:
        <?php echo $_SESSION['degree']; ?>
      </h2>

      <h2>Department:
        <?php echo $_SESSION['department']; ?>
      </h2>
    </main>

    <footer>Copyright (c) 2018 Copyright Holder All Rights Reserved.</footer>
  </body>
</html>
