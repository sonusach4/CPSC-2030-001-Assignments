<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet/less" type="text/css" media="screen" href="login.less" />
    <link rel="stylesheet/less" type="text/css" media="screen" href="styles.less" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/less.js/3.7.1/less.min.js"></script>
    <title>Patient Signup - Walsh Institute</title>
  </head>
  <body>

    <main>

      <section class="login">
        <a href="index.php">Homepage</a>
      </section>

      <div class="container">
        <h2>Signup</h1>
        <div class="signup-form">
          <form action="signup.inc.php" method="post">
            <label class="username" for="userame"><b>Username</b></label>
            <input class="username" type="text" name="username" placeholder="Username">

            <label class="firstname" for="firstname"><b>First Name</b></label>
            <input class="firstname" type="text" name="firstname" placeholder="First Name">

            <label class="lastname" for="lastname"><b>Last Name</b></label>
            <input class="lastname" type="text" name="lastname" placeholder="Last Name">

            <label class="phone" for="phone"><b>Phone Number</b></label>
            <input class="phone" type="text" name="phone" placeholder="Phone Number">

            <label class="address" for="address"><b>Address</b></label>
            <input class="address" type="text" name="address" placeholder="Address">

            <label class="password" for="password"><b>Password</b></label>
            <input class="password" type="password" name="password" placeholder="Password">

            <label class="checkpassword" for="checkpassword"><b>Password Again</b></label>
            <input class="checkpassword" type="password" name="checkpassword" placeholder="Enter your password again">

            <button type="submit" name="signup">Sign Up</button>
            <a  class="signup-link" href="patient-login.php">Login</a>
          </form>
        </div>
      </div>

    </main>
  </body>
</html>
