<?php
  session_start();

?>

<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
    <script src="https://use.fontawesome.com/90dc64b761.js"></script>
    <link rel="stylesheet/less" type="text/css" media="screen" href="styles.less" />
    <script src="script.js" defer></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/less.js/3.0.2/less.min.js" ></script>
    <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
        crossorigin="anonymous"></script>
    <link rel="icon" type="image/x-icon"  href="Walsh-icon.png">
    <!-- <script>
      $(document).ready(function() {
        $("form").submit(function(event) {
          event.preventDefault();
          var problem = $("problem").val();
          var doctor = $("doctor").val();
          var comment = $("comment").val();
          $(".message").load("appointment.php", { problem: problem, doctor: doctor, comment: comment });
        });
      });
    </script> -->
    <title>My Appointments - Walsh Institute</title>
  </head>
  <body>

    <header>
      <div class="heading">
        <h1> Walsh Institute</h1>
        <h3><em>"A Transforming, Healing Presence"</em></h3>
      </div>
    </header>

    <nav>
      <a href="patient-login-success.php"><i class="far fa-calendar-check" aria-hidden="true"></i>Book Appointment</a>
      <a href="patient-appointments.php"><i class="far fa-calendar-check" aria-hidden="true"></i>My Appointments</a>
      <a href="details.php"><i class="fas fa-user-md" aria-hidden="true"></i>Update Profile</a>
      <a href="patient-contact.php"><i class="fas fa-address-book" aria-hidden="true"></i>Contact Us</a>
    </nav>

    <section class="login">
      <a href="#">Welcome <?php echo $_SESSION['firstname']." ".$_SESSION['lastname'] ?></a>

      <form class="logout-button" action="logout.inc.php" method="post">
        <button type="submit" name="logout-button">Logout</button>
      </form>

    </section>

    <main class="page appointment-form">

      <?php
      require'db.inc.php';
      $patient = $_SESSION['username'];
      $sql = "SELECT * FROM appointment WHERE patient_username='$patient'";
      $result = mysqli_query($conn, $sql);

      if($result) {
        $table = $result->fetch_all(MYSQLI_ASSOC);

        echo "<table>";
        echo "<tr><th>Doctor Username</th><th>Patient Username</th><th>Problem</th><th>Day</th><th>Comments</th></tr>";

        foreach ($table as $row) {
          echo "<tr><td>".$row["doctor_username"]."</td><td>".$row["patient_username"]."</td><td>".$row["problem"]."</td><td>".$row['day']."</td><td>".$row['comment']."</td>";
        }
        echo "</table>";
      }else {
        echo "Error";
      }
      ?>

    </main>

  </body>
</html>
