<?php

// Checking if the user pressed the login button
if(isset($_POST['login'])) {
  // Making a connection to database using the file
  require 'db.inc.php';

  // Getting the values from the form and storing them to variables
  $username = $_POST['username'];
  $password = $_POST['password'];

  // Checking if the user left the fields empty
  // Now I restricted the user from leaving the fields empty by adding 'required' keyword to input blocks
  if(empty($username) || empty($password)) {
    header("Location: doctor-login.php?error=empty-fields");
    exit();
  }
  else {
    // Getting the values from the database
    $sql = "SELECT * FROM doctor WHERE username=?";
    $stmt = mysqli_stmt_init($conn);

    if(!mysqli_stmt_prepare($stmt, $sql)) {
      header("Location: doctor-login.php?error=sql-error");
      exit();
    }
    else {
      mysqli_stmt_bind_param($stmt, "s", $username);
      mysqli_stmt_execute($stmt);

      $result = mysqli_stmt_get_result($stmt);
      if ($row = mysqli_fetch_assoc($result)) {
        $passwordMatch = password_verify($password, $row['password']);

        if ($passwordMatch == false) {
          header("Location: doctor-login.php?error=password-does-not-match");
          exit();
        }
        else if ($passwordMatch == true) {
          // Login the user to main website
          // Startig the session
          session_start();

          $_SESSION['username'] = $row['username'];
          $_SESSION['userID'] = $row['ID'];
          $_SESSION['firstname'] = $row['firstname'];
          $_SESSION['lastname'] = $row['lastname'];
          $_SESSION['phone'] = $row['phone'];
          $_SESSION['degree'] = $row['degree'];
          $_SESSION['department'] = $row['department'];

          header("Location: doctor-login-success.php?login=success");
          exit();
        }
      }
      else {
        header("Location: doctor-login.php?error=no-user-found");
        exit();
      }
    }
  }
}
else {
  header("Location: doctor-login.php");
  exit();
}
