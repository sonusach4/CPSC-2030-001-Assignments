-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 04, 2018 at 08:15 AM
-- Server version: 10.1.36-MariaDB
-- PHP Version: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `testproject2`
--

-- --------------------------------------------------------

--
-- Table structure for table `appointment`
--
CREATE DATABASE testproject2;
USE testproject2;
CREATE TABLE `appointment` (
  `doctor_username` varchar(40) DEFAULT NULL,
  `patient_username` varchar(40) DEFAULT NULL,
  `problem` tinytext NOT NULL,
  `day` tinytext NOT NULL,
  `comment` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `appointment`
--

INSERT INTO `appointment` (`doctor_username`, `patient_username`, `problem`, `day`, `comment`) VALUES
('sonusach98', 'sonusach', 'Face Transplant', 'December 07, 2018', 'Face Transplant'),
('sonusach98', 'gagandeep', 'Face Transplant', 'December 07, 2018', 'Need a face transplant'),
('narinder98', 'gagandeep', 'Hair Transplant', '10 December, 2018', 'Need a hair transplant');

-- --------------------------------------------------------

--
-- Table structure for table `doctor`
--

CREATE TABLE `doctor` (
  `username` varchar(40) NOT NULL,
  `firstname` tinytext NOT NULL,
  `lastname` tinytext NOT NULL,
  `phone` tinytext NOT NULL,
  `degree` tinytext NOT NULL,
  `department` tinytext NOT NULL,
  `password` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `doctor`
--

INSERT INTO `doctor` (`username`, `firstname`, `lastname`, `phone`, `degree`, `department`, `password`) VALUES
('balram98', 'Dr.Balram', 'Lailna', '7894561237', 'M.B.B.S.', 'E.N.T', '$2y$10$QVDTzbf/Np8wHMvMnSNuN.RzKxfeV3CTn50p8GgjRKuJ2GmfvB4Rq'),
('gagandeep98', 'Dr.Gagandeep', 'Singh', '1234567890', 'M.B.B.S.', 'E.N.T', '$2y$10$aswwinUuV1yNv3BHEy5UbewNodHFuQRiCNQxsoFarJ2yGmm4d9bni'),
('harpreet98', 'Dr.Harpreet', 'Sangha', '7782345678', 'M.B.B.S.', 'Oncology', '$2y$10$qfRMS2p5VZiTxfdLnICeie2O6Ap9.DoFDhD0PPaiO3WAWFLgFGZjm'),
('narinder98', 'Dr.Narinder', 'Modi', '1234567890', 'M.D.', 'E.N.T', '$2y$10$pum6uh5OUqFrT3AKsDmJz.TkQ55jlLzzsVKuo8nFPUgLFmp9RCP2W'),
('prithvi98', 'Dr.Prithvi', 'Verma', '7894561230', 'M.S.', 'Oncology', '$2y$10$b.5vGaYAGgVFIG7HpdJEde/n/QrVSrO7crC788WzmsmduhMHib/1O'),
('sonusach98', 'Dr.Sourabh', 'Sachdeva', '7894561230', 'M.D. Medicine', 'Oncology', '$2y$10$9K4mIPMq10yzQ9lsL3h0ieC92GdzDBNvxMZeNHCx/oExuUYilQfu6');

-- --------------------------------------------------------

--
-- Table structure for table `patient`
--

CREATE TABLE `patient` (
  `username` varchar(40) NOT NULL,
  `firstname` tinytext NOT NULL,
  `lastname` tinytext NOT NULL,
  `phone` tinytext NOT NULL,
  `address` longtext NOT NULL,
  `password` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `patient`
--

INSERT INTO `patient` (`username`, `firstname`, `lastname`, `phone`, `address`, `password`) VALUES
('gagandeep', 'Gagandeep', 'Singh', '7783456789', '12712 AB St', '$2y$10$lbyoSAvGBISXquZt0ZEwkeAxP1ewIVsc9U34G166hYLZQXbaPcV3O'),
('sonusach', 'Sonu', 'Sachdeva', '7894561237', 'ABCD st., 88 Ave', '$2y$10$stynOQLaRsh/JsPcDEep9ud4wzrAoQVcZs8UDv87Sj9CfYYnSvFx2');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `appointment`
--
ALTER TABLE `appointment`
  ADD KEY `doctor_username` (`doctor_username`),
  ADD KEY `patient_username` (`patient_username`);

--
-- Indexes for table `doctor`
--
ALTER TABLE `doctor`
  ADD PRIMARY KEY (`username`);

--
-- Indexes for table `patient`
--
ALTER TABLE `patient`
  ADD PRIMARY KEY (`username`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `appointment`
--
ALTER TABLE `appointment`
  ADD CONSTRAINT `appointment_ibfk_1` FOREIGN KEY (`doctor_username`) REFERENCES `doctor` (`username`),
  ADD CONSTRAINT `appointment_ibfk_2` FOREIGN KEY (`patient_username`) REFERENCES `patient` (`username`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
