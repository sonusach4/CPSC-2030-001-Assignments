<?php
require_once './vendor/autoload.php';

$loader = new Twig_Loader_Filesystem('./templates'); //set to load from the ./templates directory

//Sometimes you have to manually delete the cache
$twig = new Twig_Environment($loader);

$template = $twig->load("main.twig.html");
    echo $template->render(array('pageTitle' => 'Homepage', 'mainTopic' => 'About Us'));

?>
