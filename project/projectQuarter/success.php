<?php
session_start();
if(!isset($_SESSION['login']) || $_SESSION['login'] == false) {
  header("Location: patient-login.php");
}
?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Success</title>
  </head>
  <body>
    <h1>Successfully Logged In</h1>

    <p>For this time, I am printing this message and for the final page, the coressponding userpage will be displayed and the details will be shown using twig template.</p>
  </body>
</html>
