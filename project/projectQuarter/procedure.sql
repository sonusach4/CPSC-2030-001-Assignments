DELIMITER //
CREATE PROCEDURE get_doctor (IN id INT)
BEGIN
  SELECT *
  From doctor
  WHERE ID=id;
END //
DELIMITER ;

DELIMITER //
CREATE PROCEDURE get_patient (IN id INT)
BEGIN
  SELECT *
  From patient
  WHERE ID=id;
END //
DELIMITER ;