<?php
session_start();

include "helper.php";
$servername = "localhost";
$username = "CPSC2030";
$password = "CPSC2030";
$dbname = "hospital";

// Create connection

// $ID = $_POST['usr-name'];
$conn = new mysqli($servername, $username, $password, $dbname);
// $result = $conn->query("call get_patient(\"$ID\")");
clear($conn);

// if($result) {
//   $table = $result->fetch_all(MYSQLI_ASSOC);
//
//   foreach ($table as $row) {
//     $id = $row["ID"];
//     $password = $row["PASSWORD"];
//   }
//
//   if(isset($_SESSION['login']) && $_SESSION['login'] == true) {
//     header("Location: success.php");
//   }
//
//   if(isset($_POST['usr-name']) && isset($_POST['pass'])) {
//     if($_POST['usr-name'] == $id && $_POST['pass'] == $password) {
//       $_SESSION['login'] = true;
//       header("Location: success.php");
//     }
//   }
// }
?>



<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Patient Login - Walsh Institute</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet/less" type="text/css" media="screen" href="login.less" />
    <link rel="stylesheet/less" type="text/css" media="screen" href="style.less" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/less.js/3.7.1/less.min.js"></script>
  </head>

  <body>
    <main>
      <section class="login">
        <a href="index.html">Homepage</a>
      </section>

      <div class="container">

        <form class="login-page" action="#" method="POST">
          <h2>Patient Login</h2>
          <label for="usr-name"><b>Username/ID</b></label>
          <input type="text" placeholder="Enter Username or ID" name="usr-name" required>

          <label for="pass"><b>Password</b></label>
          <input type="password" placeholder="Enter Password" name="pass" required>

          <button type="submit">Login</button>
        </form>

      </div>

      <?php
        if(isset($_POST['usr-name'])) {
          $id=$_POST['usr-name'];
          $password=$_POST['pass'];

          $result = $conn->query("call get_patient(\"$id\")");

          if(mysql_num_rows($result)==1){
            echo "Success";
          } else {
            echo "Fail";
          }
        }
        ?>
    </main>
    <footer></footer>
  </body>
</html>
