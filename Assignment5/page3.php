<!DOCTYPE html>
<html lang="en" dir="ltr">
<?php
include "sqlhelper.php";

$username = "CPSC2030";
$password = "CPSC2030";
$servername = "localhost";
$dbname = "pokedex";

$conn = new mysqli($servername, $username, $password, $dbname);
$type = mysqli_real_escape_string($conn, $_GET["type"]);

$result = $conn->query("call get_type(\"$type\")");
clearConnection($conn);
?>
  <head>
    <meta charset="utf-8">
    <title><?php echo $type ?> </title>
  </head>
  <body>
    <h1>Type: <?php echo $type ?></h1>
    <h2><a href="index.php">Back to Home Page</a></h2>
    <?php
      if($result) {
        $table = $result->fetch_all(MYSQLI_ASSOC);
        echo "<table>";
        echo "<tr><th>No.</th><th>Name</th><th>Type</th><th>Type2</th></tr>";
        foreach($table as $row) {
          $link = "pokemon.php?pokemon=".urlencode($row["NAME"])."&number=".urlencode($row["NAT_NO"]);
          $link2 = "page3.php?type=".urlencode($row["TYPE"]);
          echo "<tr><td>".$row["NAT_NO"]."</td><td><a href= '$link'>";
          echo $row["NAME"];
          echo "</a></td>";
          echo "<td><a href= '$link2'>";
          echo $row["TYPE"];
          echo "</a></td><td>".$row["TYPE2"]."</tr>";
        }
        echo "</table>";
      } else {
        echo "Error";
      }
    ?>
  </body>
</html>
