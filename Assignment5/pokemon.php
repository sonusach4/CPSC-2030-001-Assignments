<!DOCTYPE html>
<html lang="en" dir="ltr">
<?php
include "sqlhelper.php";

$username = "CPSC2030";
$password = "CPSC2030";
$servername = "localhost";
$dbname = "pokedex";

$conn = new mysqli($servername, $username, $password, $dbname);
$name = mysqli_real_escape_string($conn, $_GET["pokemon"]);
$number = mysqli_real_escape_string($conn, $_GET["number"]);

$result = $conn->query("call get_values(\"$name\")");
clearConnection($conn);

$conn1 = new mysqli($servername, $username, $password, $dbname);
$result1 = $conn1->query("call weak(\"$name\")");
clearConnection($conn1);

$conn2 = new mysqli($servername, $username, $password, $dbname);
$result2 = $conn2->query("call strong(\"$name\")");
clearConnection($conn2);

$conn3 = new mysqli($servername, $username, $password, $dbname);
$result3 = $conn3->query("call vulnerable(\"$name\")");
clearConnection($conn3);

$conn4 = new mysqli($servername, $username, $password, $dbname);
$result4 = $conn4->query("call resistant(\"$name\")");
clearConnection($conn4);

?>
  <head>
    <meta charset="utf-8">
    <title><?php echo $name ?></title>
  </head>


  <body>

    <h1>Pokemon Name: <?php echo $name; ?></h1>
    <h2><a href="index.php">Back to Home Page</a></h2>

    <?php
    echo "<h3>Properties of ".$name."</h3>";

        echo "<p> Number : ".$number."</p>";
        echo "<p> Name : ".$name."</p>";

        if($result) {

          $table = $result->fetch_all(MYSQLI_ASSOC);

          foreach ($table as $row) {
            echo "<p>HOENN_NO : ".$row["HOENN_NO"]."</p>";
            echo "<p>TYPE : ".$row["TYPE"]."</p>";
            echo "<p>TYPE2 : ".$row["TYPE2"]."</p>";
            echo "<p>HP : ".$row["HP"]."</p>";
            echo "<p>ATK : ".$row["ATK"]."</p>";
            echo "<p>DEF : ".$row["DEF"]."</p>";
            echo "<p>SAT : ".$row["SAT"]."</p>";
            echo "<p>SDF : ".$row["SDF"]."</p>";
            echo "<p>SPD : ".$row["SPD"]."</p>";
            echo "<p>BST : ".$row["BST"]."</p>";
          }
        }

        if($result1 && $result2 && $result3 && $result4) {
          $table = $result1->fetch_all(MYSQLI_ASSOC);
          $table1 = $result2->fetch_all(MYSQLI_ASSOC);
          $table2 = $result3->fetch_all(MYSQLI_ASSOC);
          $table3 = $result4->fetch_all(MYSQLI_ASSOC);

          echo "<p>Weak against: ";
          foreach ($table as $row) {
            echo $row["TYPE2"].", ";
          }
          echo "</p>";

          echo "<p>Strong against: ";
          foreach ($table1 as $row) {
            echo $row["TYPE2"].", ";
          }
          echo "</p>";

          echo "<p>Vulnerable to: ";
          foreach ($table2 as $row) {
            echo $row["TYPE2"].", ";
          }
          echo "</p>";

          echo "<p>Resistant to: ";
          foreach ($table3 as $row) {
            echo $row["TYPE2"].", ";
          }
          echo "</p>";
        } else {
          echo "error";
        }

    ?>


  </body>
</html>
