/* one function to get all the pokemon */
DELIMITER //
CREATE PROCEDURE get_pokemon ()
BEGIN
  SELECT *
  From pokemon;
END //
DELIMITER ;

DELIMITER //
CREATE PROCEDURE get_values
(IN str text)
BEGIN
  SELECT *
  From pokemon AS poke
  WHERE poke.NAME = str;
END //
DELIMITER ;

DELIMITER //
CREATE PROCEDURE weak
(IN str text)
BEGIN
  SELECT *
  From pokemon as poke
  INNER JOIN weak_against ON weak_against.TYPE = poke.TYPE OR weak_against.TYPE = poke.TYPE2
  WHERE poke.NAME = str;
END //
DELIMITER ;

DELIMITER //
CREATE PROCEDURE strong
(IN str text)
BEGIN
  SELECT *
  From pokemon as poke
  INNER JOIN strong_against ON strong_against.TYPE = poke.TYPE OR strong_against.TYPE = poke.TYPE2
  WHERE poke.NAME = str;
END //
DELIMITER ;

DELIMITER //
CREATE PROCEDURE vulnerable
(IN str text)
BEGIN
  SELECT *
  From pokemon as poke
  INNER JOIN vulnerable_to ON vulnerable_to.TYPE = poke.TYPE OR vulnerable_to.TYPE = poke.TYPE2
  WHERE poke.NAME = str;
END //
DELIMITER ;

DELIMITER //
CREATE PROCEDURE resistant
(IN str text)
BEGIN
  SELECT *
  From pokemon as poke
  INNER JOIN resistant_to ON resistant_to.TYPE = poke.TYPE OR resistant_to.TYPE = poke.TYPE2
  WHERE poke.NAME = str;
END //
DELIMITER ;

DELIMITER //
CREATE PROCEDURE get_type
(IN str text)
BEGIN
  SELECT *
  From pokemon AS poke
  WHERE poke.TYPE = str;
END //
DELIMITER ;
