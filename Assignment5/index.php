<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Pokedex</title>
  </head>

  <?php
  include "sqlhelper.php";
  $servername = "localhost";
  $username = "CPSC2030";
  $password = "CPSC2030";
  $dbname = "pokedex";

  // Create connection
  $conn = new mysqli($servername, $username, $password, $dbname);
  $result = $conn->query("call get_pokemon()");
  clearConnection($conn);

  ?>

  <body>
    <h1>This is pokedex</h1>
    <h2><a href="index.php">Home Page</a></h2>
    <?php
    if($result) {
      $table = $result->fetch_all(MYSQLI_ASSOC); //returns a table or rows. Rows are associative arrays

      echo "<table>";
      echo "<tr><th>No.</th><th>Name</th><th>Type</th><th>Type2</th></tr>";
      foreach($table as $row) {
        $link = "pokemon.php?pokemon=".urlencode($row["NAME"])."&number=".urlencode($row["NAT_NO"]);
        $link2 = "page3.php?type=".urlencode($row["TYPE"]);
        echo "<tr><td>".$row["NAT_NO"]."</td><td><a href= '$link'>";
        echo $row["NAME"];
        echo "</a></td>";
        echo "<td><a href= '$link2'>";
        echo $row["TYPE"];
        echo "</a></td><td>".$row["TYPE2"]."</tr>";
      }
      echo "</table>";
    } else {
      echo "Error";
    }
    ?>

  </body>
</html>
