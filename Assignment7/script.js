let idd;
let squadLength=0;
let squad = [];
let i=0,j=0;
function Character(name,side,unit,rank,role,description,image) {
  this.name = name;
  this.side = side;
  this.unit = unit;
  this.rank = rank;
  this.role = role;
  this.description = description;
  this.image = image;
}

let claude = new Character("Claude Wallace","Edinburgh Army","Ranger Corps, Squad E","First Lieutenant","Tank Commander","Born in the Gallian city of Hafen, this promising young squad leader is keenly attuned to climate changes and weather fluctuations. Ever since graduating as valedictorian from the Royal Military Academy, his talent and determination have been an inspiration to his subordinates.","chara-ss01.jpg");
let riley = new Character("Riley Miller","Edinburgh Army","Federate Joint Ops","Second Lieutenant","Artillery Advisor","Born in the Gallian city of Hafen, this brilliant inventor was assigned to Squad E after researching ragnite technology in the United States of Vinland. She appears to share some history with Claude, although the memories seem to be traumatic ones.","chara-ss02.jpg");
let raz = new Character("Raz","Edinburgh Army","Ranger Corps, Squad E","Sergeant","Fireteam Leader","Born in the Gallian city of Hafen, this foul-mouthed Darcsen worked his way up from the slums to become a capable soldier. Though foul-mouthed and reckless, his athleticism and combat prowess is top-notch... And according to him, he's invincible.","chara-ss03.jpg");
let kai = new Character("Kai Schulen","Edinburgh Army","Ranger Corps, Squad E","Sergeant Major","Fireteam Leader","Born in the Gallian city of Hafen, this cool and collected sharpshooter has earned the codename &quot;Deadeye Kai.&quot; Along with her childhood friends, she joined a foreign military to take the fight to the Empire. She loves fresh-baked bread, almost to a fault.","chara-ss04.jpg");
let ronald = new Character("Ronald Albee","Edinburgh Army","Ranger Corps, Squad F","Second Lieutenant","Tank Operator","Born in the United Kingdom of Edinburgh, this stern driver was Minerva Victor's underclassman at the military academy. Upon being assigned to Squad F, he swore an oath of fealty to Lt. Victor, and takes great satisfaction in upholding her chivalric code.","chara-ss17.jpg");
let minerva = new Character("Minerva Victor","Edinburgh Army","Ranger Corps, Squad F","First Lieutenant","Senior Commander","Born in the United Kingdom of Edinburgh to a noble family, this competitive perfectionist has authority over the 101st Division's squad leaders. She values honor and chivalry, though a bitter rivalry with Lt. Wallace sometimes compromises her lofty ideals.","chara-ss11.jpg");
let karen = new Character("Karen Stuart","Edinburgh Army","Squad E","Corporal","Combat EMT","Born as the eldest daughter of a large family, this unflappable field medic is an expert at administering first aid in the heat of battle. Although she had plans to attend medical school, she instead enlisted in her nation's military to support her growing household.","chara-ss12.jpg");
let ragnarok = new Character("Ragnarok","Edinburgh Army","Squad E","K-9 Unit","Mascot","Once a stray, this good good boy is lovingly referred to as &quot;Rags.&quot;As a K-9 unit, he's a brave and intelligent rescue dog who's always willing to lend a helping paw. When the going gets tough, the tough get ruff.","chara-ss13.jpg");
let miles = new Character("Miles Arbeck","Edinburgh Army","Ranger Corps, Squad E","Sergeant","Tank Operator","Born in the United Kingdom of Edinburgh, this excitable driver was Claude Wallace's partner in tank training, and was delighted to be assigned to Squad E. He's taken up photography as a hobby, and is constantly taking snapshots whenever on standby.","chara-ss15.jpg");
let dan = new Character("Dan Bentley","Edinburgh Army","Ranger Corps, Squad E","Private First Class","APC Operator","Born in the United States of Vinland, this driver loves armored personnel carriers with a passion. His skill behind the wheel is matched only by his way with a wrench. Though not much of a talker, he takes pride in carrying his teammates through combat.","chara-ss16.jpg");

let people = [claude,riley,raz,kai,ronald,minerva,karen,ragnarok,miles,dan];

function displayProfile() {
  // console.log(idd);
  // console.log(people[idd].name);
  document.getElementById('name').innerHTML = "Name: "+people[idd].name;
  document.getElementById('side').innerHTML = "Side: "+people[idd].side;
  document.getElementById('rank').innerHTML = "Rank: "+people[idd].rank;
  document.getElementById('unit').innerHTML = "Unit: "+people[idd].unit;
  document.getElementById('role').innerHTML = "Role: "+people[idd].role;
  document.getElementById('description').innerHTML = people[idd].description;
  document.getElementById('picture').innerHTML = "<img src="+people[idd].image+" >";
}


function getFirstName(person) {
  let name="";
  for(let i=0; i < person.name.length; i++) {
    if(person.name.charAt(i) == " ") {
      break;
    } else {
      name += person.name.toLowerCase().charAt(i);
    }
  }
  return name;
}


function getID(event) {
   let tag = event.target;
   idd = tag.id;
   // console.log(idd);
 }

function changeProperty(idd) {
  document.getElementById(idd).style.color = "red";
  addToSquad(idd);
}

function removeFromSquad(j) {
  squad.splice(j,1);
}
function addToSquad(idd) {

  if(squad.length <= 4 && !notPresent(idd)) {
      squad.push(people[idd]);

    if(squad.length != 0) {

        let info = document.createElement("DIV");
        info.className = 'squadPerson';
        info.id = j;
        info.setAttribute("onclick","removeFromSquad(j)")
        let info_text = document.createTextNode(people[idd].name);
        info.appendChild(info_text);
        document.getElementById('squad').appendChild(info);
        j++;

    }
  }
}

function notPresent(idd) {
  for(let i=0; i < squad.length; i++) {
    if(people[idd].name.localeCompare(squad[i].name) == 0)
      return true;
  }
  return false;
}
for(let person of people) {

  let info = document.createElement("DIV");
  info.className = 'person';
  info.id = i;
  // info.setAttribute("onclick","addToSquad()");
  info.setAttribute("onclick","changeProperty(idd)");
  info.setAttribute("onmouseover","getID(event),displayProfile()");

  // info.setAttribute("onmousedown","getID(event)","changeColor()");
  let info_text = document.createTextNode(person.name);

  info.appendChild(info_text);
  document.getElementById('battalion').appendChild(info);
  i++;
}
